package utils;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

public class BindingUtils {
    /**
     * @param url url de l'image
     * @return une image en fonction de l'URL de l'image
     */
    public static ObjectProperty<Image> imageProperty(String url) {return new SimpleObjectProperty<>(new Image(url));}
}
