package model.business.data;

import model.HistoryModel;
import model.PlayerModel;
import model.business.Board;
import model.business.Game;
import model.business.Graveyard;

/**
 * Intergace qui défini les methode de persistance
 */
public interface IDataManager {
    /**
     * permet de charger une game
     * @return une game initialisée
     */
    Game load();

    /**
     * permet de sauvegarder une game
     * @param g la game a sauvegarder
     */
    void save(Game g);
}
