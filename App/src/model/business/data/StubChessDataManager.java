package model.business.data;

import model.HistoryModel;
import model.PlayerModel;
import model.business.Board;
import model.business.Graveyard;
import model.business.Player;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Classe concrète qui exécute le patron de méthode dans ChessDataManager
 */
public class StubChessDataManager extends ChessDataManager {
    /**
     * Permet de générer un plateau depuis le stub
     * @return un board générer par factory
     */
    @Override
    public Board loadBoard() {
        BoardFactory bf = new BoardFactory();
        return bf.chessBoardFactory();
    }

    /**
     * N'A PAS DE SANS EN STUB
     * @param b le plateau a sauvegarder
     */
    @Override
    public void saveBoard(Board b) {
        throw new UnsupportedOperationException("Cannot css stub generated data");
    }

    /**
     * Permet de retourner un historique depuis un stu
     * @return l'historique
     */
    @Override
    public HistoryModel loadHistory() {
        return new HistoryModel();
    }

    /**
     * N'A PAS DE SENS EN STUB
     * @param h l'historique a sauvegarder
     */
    @Override
    public void saveHistory(HistoryModel h) {
        throw new UnsupportedOperationException("Cannot css stub generated data");
    }

    /**
     * Permet de générer un player model depuis un stub
     * @return le player model
     */
    @Override
    public PlayerModel loadPlayers() {
        return new PlayerModel(new Player("Player 1"), new Player("Player 2"));
    }

    /**
     * N'A PAS DE SENS EN STUB
     * @param p le player model a sauvegarder
     */
    @Override
    public void savePlayers(PlayerModel p) {
        throw new UnsupportedOperationException("Cannot css stub generated data");
    }

    /**
     * Permet de générer un cimetière depuis un stub
     * @return un graveyard
     */
    @Override
    public Graveyard loadGraveyard() {
        return new Graveyard();
    }

    /**
     * N'A PAS DE SENS EN STUB
     * @param g le graveyard a sauvegarder
     */
    @Override
    public void saveGraveyard(Graveyard g) {
        throw new UnsupportedOperationException("Cannot css stub generated data");
    }
}
