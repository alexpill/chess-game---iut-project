package model.business.data;

import controller.GameMediator;
import javafx.collections.FXCollections;
import model.HistoryModel;
import model.PlayerModel;
import model.business.Board;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import model.business.Game;
import model.business.Graveyard;

/**
 * Classe facade qui contient différente données
 */
public class Manager {

    /**
     * Contient une instance de game médiator pour le bon fonctionnement du patron médiateur
     */
    private GameMediator gameMediator;
        public GameMediator getGameMediator() {return gameMediator;}
        public void setGameMediator(GameMediator gameMediator) {this.gameMediator = gameMediator;}

    /**
     * Instance de data manager qui permet de charger ou sauvegarder des données
     */
    private IDataManager dataManager;
        public void setDataManager(IDataManager IDataManager) {this.dataManager = IDataManager;}

    /**
     * Plateau encapsulé dans une object property
     */
    private final ObjectProperty<Board> board = new SimpleObjectProperty<>();
        public Board getBoard() {return board.get();}
        public void setBoard(Board board) {this.board.set(board);}
        public ObjectProperty<Board> boardProperty() {return board;}

    /**
     * PLayer mode encapsulé dans une Object property
     */
    private final ObjectProperty<PlayerModel> playerModel = new SimpleObjectProperty<>();
        public PlayerModel getPlayerModel() {return playerModel.get();}
        public ObjectProperty<PlayerModel> playerModelProperty() {return playerModel;}
        public void setPlayerModel(PlayerModel playerModel) {this.playerModel.set(playerModel);}

    /**
     * History model encapsulé dans une object property
     */
    private final ObjectProperty<HistoryModel> historyModel = new SimpleObjectProperty<>(new HistoryModel());
        public HistoryModel getHistoryModel() {return historyModel.get();}
        public ObjectProperty<HistoryModel> historyModelProperty() {return historyModel;}
        public void setHistoryModel(HistoryModel historyModel) {this.historyModel.set(historyModel);}

    /**
     * Graveyard encapsulé dans une object property
     */
    private final ObjectProperty<Graveyard> graveyard = new SimpleObjectProperty<>();
        public Graveyard getGraveyard() {return graveyard.get();}
        public ObjectProperty<Graveyard> graveyardProperty() {return graveyard;}
        public void setGraveyard(Graveyard graveyard) {this.graveyard.set(graveyard);}

    /**
     * Permet de setter les différents attribut en fonction de ce que renvoit le dataManager
     */
    public void load(){
        if(dataManager != null){
            Game g = dataManager.load();
            if(g == null){
                setBoard(null);
                setHistoryModel(null);
                setPlayerModel(null);
                setGraveyard(null);
            }else{
                setBoard(g.getBoard());
                setHistoryModel(g.getHistory());
                setPlayerModel(g.getPlayers());
                setGraveyard(g.getGraveyard());
                if(getGraveyard().getDicoGraveyard().size() == 0){
                    getGraveyard().getDicoGraveyard().put(getPlayerModel().getP1(), FXCollections.observableArrayList());
                    getGraveyard().getDicoGraveyard().put(getPlayerModel().getP2(), FXCollections.observableArrayList());
                }
            }
        }
    }

    /**
     * Permet de sauvegarder les données du manager via le dataManager
     */
    public void save(){
        if (dataManager != null){
            dataManager.save(new Game(getBoard(),getHistoryModel(),getPlayerModel(),getGraveyard()));
        }
    }

    /**
     * Constructeur vide pour une serialisation XML
     */
    public Manager(){}

    /**
     * Constructeur 1 paramètre
     * @param dm le data manager
     */
    public Manager(IDataManager dm){
        setDataManager(dm);
        setGameMediator(null);
        if(dataManager != null){
            load();
        }
    }


    /**
     * Constructeur 2 paramètres
     * @param dm le data manager
     * @param gm de game médiator (pour le patron médiateur
     */
    public Manager(IDataManager dm, GameMediator gm){
            setDataManager(dm);
            setGameMediator(gm);
            if(dataManager != null){
               load();
            }
    }
}
