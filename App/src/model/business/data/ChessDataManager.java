package model.business.data;

import model.HistoryModel;
import model.PlayerModel;
import model.business.Board;
import model.business.Game;
import model.business.Graveyard;

import java.io.IOException;

/**
 * Implémente l'interface data manager et défini un patron de méthode sur les méthode load et save
 */
public abstract class ChessDataManager implements IDataManager {

    @Override
    public Game load() {
        Game g = new Game();
        g.setBoard(loadBoard());
        g.setHistory(loadHistory());
        g.setPlayers(loadPlayers());
        g.setGraveyard(loadGraveyard());

        return g;
    }

    @Override
    public void save(Game g) {
        saveBoard(g.getBoard());
        saveHistory(g.getHistory());
        savePlayers(g.getPlayers());

    }

    public abstract Board loadBoard();
    public abstract void saveBoard(Board b);

    public abstract HistoryModel loadHistory();
    public abstract void saveHistory(HistoryModel h);

    public abstract PlayerModel loadPlayers();
    public abstract void savePlayers(PlayerModel p);

    public abstract Graveyard loadGraveyard();
    public abstract void saveGraveyard(Graveyard g);
}
