package model.business.data;

import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import model.HistoryModel;
import model.PlayerModel;
import model.business.*;
import model.business.chess.ChessBoard;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XMLChessDataManager extends ChessDataManager {

    /**
     * Nom du fichier de sauvegarde et de lecture
     */
    private static final String FILE = "game.xml";
    private XMLEncoder encoder;
    private XMLDecoder decoder;


    /**
     * Permet de redéfinir la méthode dans ChessDataManager de sorte a éviter l'ouverture multiple du fichier
     * @return une game chargée
     */
    @Override
    public Game load() {
        Game g = new Game();
        try {
            decoder = new XMLDecoder(new FileInputStream(FILE));
            g.setBoard(loadBoard());
            g.setHistory(loadHistory());
            g.setPlayers(loadPlayers());
            g.setGraveyard(loadGraveyard());
            decoder.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return g;
    }

    /**
     * Permet de redéfinir la méthode dans ChessDataManager de sorte a éviter l'ouverture multiple du fichier
     * @param g une game sauvegarder
     */
    @Override
    public void save(Game g) {
        try {
            encoder = new XMLEncoder(new FileOutputStream(FILE));
            saveBoard(g.getBoard());
            saveHistory(g.getHistory());
            savePlayers(g.getPlayers());
            saveGraveyard(g.getGraveyard());
            encoder.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de charger un board depuis le fichier
     * @return le board chargé
     */
    @Override
    public Board loadBoard() {
        Board b = null;
        Map<Coordinate, ObjectProperty<Piece>> dicoCoord = null;
        dicoCoord = (HashMap)decoder.readObject();
        b = new ChessBoard(FXCollections.observableHashMap());
        b.getDicoCoord().putAll(dicoCoord);
        return b;
    }

    /**
     * Permet de sauvergarder un board dans un fichier
     * @param b le board a sauvegarder
     */
    @Override
    public void saveBoard(Board b) {
        Map<Coordinate, ObjectProperty<Piece>> dicoCoord = new HashMap<>(b.getDicoCoord());
        encoder.writeObject(dicoCoord);
        encoder.flush();
    }

    /**
     * Permet de chargé un historique depuis un fichier
     * @return l'historique chargé
     */
    @Override
    public HistoryModel loadHistory() {
        HistoryModel model = null;
        List<HistoryElement> list = null;
        list = (ArrayList)decoder.readObject();
        model = new HistoryModel();
        model.setHistory(FXCollections.observableArrayList(list));
        return model;
    }

    /**
     * Permet de sauvegarder un historique dans un fichier
     * @param h l'historique a sauvegarder
     */
    @Override
    public void saveHistory(HistoryModel h) {
        List<HistoryElement> list = new ArrayList<>(h.getHistory());
        encoder.writeObject(list);
        encoder.flush();

    }

    /**
     * Permet de charger un player model depuis un fichier
     * @return le player model chargé
     */
    @Override
    public PlayerModel loadPlayers() {
        PlayerModel p = null;
        List<HistoryElement> list = null;
        p = (PlayerModel)decoder.readObject();
        return p;
    }

    /**
     * Permet de sauvegarder un player model dans un fichier
     * @param p le player model a sauvegarder
     */
    @Override
    public void savePlayers(PlayerModel p) {
        encoder.writeObject(p);
        encoder.flush();

    }

    /**
     * Permet de charger un graveyard depuis un fichier
     * @return le graveyard
     */
    @Override
    public Graveyard loadGraveyard() {
        Graveyard g = null;
        Map<Player, List<Piece>> dico = null;
        ObservableMap<Player, ObservableList<Piece>> dicoGraveyard = FXCollections.observableHashMap();
        dico = (Map<Player, List<Piece>>)(decoder.readObject());
        for(Player p :dico.keySet()){
            dicoGraveyard.put(p,FXCollections.observableArrayList(dico.get(p)));
        }
        g = new Graveyard(dicoGraveyard);
        return g;

    }

    /**
     * Permet de sauvegarder un graveyard dans un fichier
     * @param g le graveyard a sauvegarder
     */
    @Override
    public void saveGraveyard(Graveyard g) {
        Map<Player, List<Piece>> dico = new HashMap<>();
        for(Player p : g.getDicoGraveyard().keySet()){
            dico.put(p,new ArrayList<>(g.getDicoGraveyard().get(p)));
        }
        encoder.writeObject(dico);
        encoder.flush();

    }
}
