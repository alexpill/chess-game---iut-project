package model.business.data;

import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.chess.ChessBoard;
import model.business.chess.ChessCoordinate;
import model.business.chess.chesspieces.*;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import static model.business.PieceColor.BLACK;
import static model.business.PieceColor.WHITE;

/**
 * Usine d'un chess board board
 */
public class BoardFactory {
    private static final int ROW_MAX = ChessCoordinate.ROW_MAX;
    private static final int ROW_MIN = ChessCoordinate.ROW_MIN;

    public Board chessBoardFactory()
    {
        ObservableList<Piece> WhitePieces = FXCollections.observableArrayList();
        ObservableList<Piece> BlackPieces = FXCollections.observableArrayList();
        ObservableMap<Coordinate, ObjectProperty<Piece>> dicoCoord = FXCollections.observableHashMap();

        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"a"),new SimpleObjectProperty<>(null)); }
        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"b"), new SimpleObjectProperty<>(null)); }
        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"c"),new SimpleObjectProperty<>(null)); }
        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"d"),new SimpleObjectProperty<>(null)); }
        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"e"),new SimpleObjectProperty<>(null)); }
        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"f"),new SimpleObjectProperty<>(null)); }
        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"g"),new SimpleObjectProperty<>(null)); }
        for(int i = ROW_MIN; i <= ROW_MAX ; i++){ dicoCoord.putIfAbsent(new ChessCoordinate(i,"h"),new SimpleObjectProperty<>(null)); }

        //Adding a king for each color
        WhitePieces.add(new King(WHITE));
        BlackPieces.add(new King(BLACK));

        //Adding a queen for each color
        WhitePieces.add(new Queen(WHITE));
        BlackPieces.add(new Queen(BLACK));

        //Adding 2 bishops for white color
        WhitePieces.add(new Knight(WHITE,new ChessCoordinate(1,"b")));
        WhitePieces.add(new Knight(WHITE,new ChessCoordinate(1,"g")));

        //Adding 2 knights for black color
        BlackPieces.add(new Knight(BLACK,new ChessCoordinate(8,"b")));
        BlackPieces.add(new Knight(BLACK,new ChessCoordinate(8,"g")));

        //Adding 2 bishops for white color
        WhitePieces.add(new Bishop(WHITE,new ChessCoordinate(1,"f")));
        WhitePieces.add(new Bishop(WHITE,new ChessCoordinate(1,"c")));

        //Adding 2 bishops for black color
        BlackPieces.add(new Bishop(BLACK,new ChessCoordinate(8,"f")));
        BlackPieces.add(new Bishop(BLACK,new ChessCoordinate(8,"c")));

        //Adding 2 rooks for white color
        WhitePieces.add(new Rook(WHITE,new ChessCoordinate(1,"a")));
        WhitePieces.add(new Rook(WHITE,new ChessCoordinate(1,"h")));

        //Adding 2 rooks for black color
        BlackPieces.add(new Rook(BLACK,new ChessCoordinate(8,"a")));
        BlackPieces.add(new Rook(BLACK,new ChessCoordinate(8,"h")));

        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"a")));
        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"b")));
        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"c")));
        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"d")));
        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"e")));
        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"f")));
        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"g")));
        WhitePieces.add(new Pawn(WHITE,new ChessCoordinate(2,"h")));

        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"a")));
        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"b")));
        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"c")));
        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"d")));
        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"e")));
        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"f")));
        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"g")));
        BlackPieces.add(new Pawn(BLACK,new ChessCoordinate(7,"h")));

        for( Piece p : WhitePieces){
            dicoCoord.put(p.getCoordinate(),new SimpleObjectProperty<>(p));
        }

        for( Piece p : BlackPieces){dicoCoord.put(
                p.getCoordinate(),new SimpleObjectProperty<>(p));
        }

        return new ChessBoard(dicoCoord);

    }

}
