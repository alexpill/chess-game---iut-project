package model.business;

/**
 * Enumération contenant les couleur d'un jeu d'échec
 */
public enum PieceColor {
    WHITE ("White"),
    BLACK ("Black");

    private final String color;

    PieceColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return color;
    }
}
