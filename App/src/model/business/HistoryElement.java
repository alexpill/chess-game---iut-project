package model.business;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Classe contenant les données relative à un élément d'historique
 */
public class HistoryElement {
    /**
     * La piece qui a bougé encapsulée dans une object property
     */
    private ObjectProperty<Piece> thePiece = new SimpleObjectProperty<>();
        public Piece getThePiece() {return thePiece.get();}
        public void setThePiece(Piece aPiece) {this.thePiece.set(aPiece);}
        public ObjectProperty<Piece> aPieceProperty() {return thePiece;}

    /**
     * La piece qui a été prise (si il y a eu une prise) encapsulée dans une object property
     */
    private ObjectProperty<Piece> thePieceTaken = new SimpleObjectProperty<>();
        public Piece getThePieceTaken() {return thePieceTaken.get();}
        public void setThePieceTaken(Piece thePieceTaken) {this.thePieceTaken.set(thePieceTaken);}
        public ObjectProperty<Piece> thePieceTakenProperty() {return thePieceTaken;}

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public HistoryElement(){
    }

    /**
     * Constructeur à 1 paramètre
     * @param piece la piece concerné par un mouvement
     */
    public HistoryElement(Piece piece){
        setThePiece(piece);
        setThePieceTaken(null);
    }

    /**
     * Constructeur à 2 paramètre
     * @param piece la piece concerné par un mouvement
     * @param taken la piece prise lors d'un mouvement
     */
    public HistoryElement(Piece piece, Piece taken){
        setThePiece(piece);
        setThePieceTaken(taken);
    }

    @Override
    public String toString() {
        return "HistoryElement{" +
                "aPiece=" + getThePiece() +
                ", thePieceTaken=" + getThePieceTaken() +
                '}';
    }
}
