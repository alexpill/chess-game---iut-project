package model.business;

import model.HistoryModel;
import model.PlayerModel;

/**
 * Classe permettant le stockage intermédaire de données
 */
public class Game {

    /**
     * Un board
     */
    private Board board = null;
    public Board getBoard() {return board;}
    public void setBoard(Board board) {this.board = board;}

    /**
     * Un historique
     */
    private HistoryModel history = null;
    public HistoryModel getHistory() {return history;}
    public void setHistory(HistoryModel history) {this.history = history;}

    /**
     * PLayer model qui gère les deux player
     */
    private PlayerModel players = null;
    public PlayerModel getPlayers() {return players;}
    public void setPlayers(PlayerModel players) {this.players = players;}

    /**
     * Cimetière
     */
    private Graveyard graveyard = null;
    public Graveyard getGraveyard() {return graveyard;}
    public void setGraveyard(Graveyard graveyard) {this.graveyard = graveyard;}

    /**
     * COnstructeur vide pour la serialisation XML
     */
    public Game(){}

    /**
     * Contructeur tout paramètres
     * @param board le plateau
     * @param history l'historique
     * @param players le player model
     * @param graveyard le cimetière
     */
    public Game(Board board, HistoryModel history, PlayerModel players, Graveyard graveyard) {
        this.board = board;
        this.history = history;
        this.players = players;
        this.graveyard = graveyard;
    }
}
