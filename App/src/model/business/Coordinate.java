package model.business;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.*;
import java.util.Objects;

/**
 * Correspond au coordonnées, version abstraite
 */
public abstract class Coordinate implements Serializable{

    /**
     * Ligne minimal
     */
    private static final int MIN_ROW = 0;

    /**
     * Colum minimal
     */
    private static final String MIN_COLUMN = "z";

    /**
     * Indice de la ligne encapsulé dans un integer property
     */
    private final IntegerProperty row = new SimpleIntegerProperty();
        public int getRow() {return row.get();}
        public void setRow(int row) {this.row.set(row);}
        public IntegerProperty rowProperty() {return row;}

    /**
     * Indice de la column encapsulé dans une String property
     */
    private final StringProperty column = new SimpleStringProperty();
        public String getColumn() {return column.get();}
        public StringProperty columnProperty() {return column;}
        public void setColumn(String column) {this.column.set(column);}

    /**
     * Constructeur par defaut pour la serialisation XML
     */
    public Coordinate()
    {
        setRow(MIN_ROW);
        setColumn(MIN_COLUMN);
    }

    /**
     * Constructeur 2 paramètres
     * @param row indice de la ligne
     * @param columns indice de la colonne
     */
    public Coordinate(int row,String columns)
    {
        setRow(row);
        setColumn(columns);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return getRow() == that.getRow() &&
                Objects.equals(getColumn(), that.getColumn());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRow(), getColumn());
    }

    @Override
    public String toString(){
        String text=getColumn();
        text=text+getRow();
        return text;
    }


}
