package model.business;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;

import java.io.Serializable;
import java.util.List;

/**
 * Classe métier des pieces
 */
public abstract class Piece implements IMovable, Serializable {

    /**
     * Nom de la piece encapsulé dans une string property
     */
    private final StringProperty name = new SimpleStringProperty();
        public String getName() {return name.get();}
        public StringProperty nameProperty() {return name;}
        public void setName(String name) {this.name.set(name);}

    /**
     * Couleur de la piece encapsulée dans une object property
     */
    private final ObjectProperty<PieceColor> pieceColor = new SimpleObjectProperty<>();
        public void setPieceColor(PieceColor pieceColor) {this.pieceColor.set(pieceColor);}
        public PieceColor getPieceColor() {return pieceColor.get();}
        public ObjectProperty<PieceColor> pieceColorProperty() {return pieceColor;}

    /**
     * Coordonnée de la piece encapsulée dans une object property
     */
    private final ObjectProperty<Coordinate> coordinate = new SimpleObjectProperty<>();
        public Coordinate getCoordinate() {return coordinate.get();}
        public void setCoordinate(Coordinate coordinate) {this.coordinate.set(coordinate);}
        public ObjectProperty<Coordinate> coordinateProperty() {return coordinate;}

    /**
     * URL de l'image de la piece encapsulée dans une object property
     */
    private StringProperty imageURL = new SimpleStringProperty();
        public String getImageURL() {return imageURL.get();}
        public StringProperty imageURLProperty() {return imageURL;}
        public void setImageURL(String imageURL) {this.imageURL.set(imageURL);}


    /**
     * Constructeur a 2 paramètre de la piece
     * @param name nom de la piece
     * @param c couleur de la piece
     */
    public Piece(String name, PieceColor c)
    {
        setName(name);
        setPieceColor(c);
        setCoordinate(null);
        setImageURL(null);
    }

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public Piece(){
    }

    /**
     * Constructeur à 3 paramètre d'une piece
     * @param name nom de la piece
     * @param c couleur de le piece
     * @param coordinate coordonnée par defaut de la piece
     */
    public Piece(String name, PieceColor c, Coordinate coordinate)
    {
        this(name,c);
        setCoordinate(coordinate);
    }


    @Override
    public String toString() {
        return getName() +" "+ getPieceColor();
    }

    /**
     * Méthode abstraite pour les mouvements de la piece
     * @param board plateau d'où veut se déplacer la piece
     * @return une liste de coordonées de déplacements possibles
     */
    public abstract List<Coordinate> getMoveFromBoard(Board board);
}
