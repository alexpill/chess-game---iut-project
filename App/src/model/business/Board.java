package model.business;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.io.Serializable;
import java.util.List;

/**
 * Board, version abstraite
 */
public abstract class Board implements Serializable {

    /**
     * Définit le nombre de colonnes/lignes maximum
     */
    private static final int SIDE_LENGHT = 8;

    /**
     * Nombre de ligne encapsulé dans une integer property
     */
    private final IntegerProperty nbRows = new SimpleIntegerProperty();
        public int getNbRows() {return nbRows.get();}
        public void setNbRows(int nbRows) {this.nbRows.set(nbRows);}
        public IntegerProperty nbRowsProperty() {return nbRows;}

    /**
     * Nombre de colonne encapsulé dans une integer property
     */
    private final IntegerProperty nbColumns = new SimpleIntegerProperty();
        public int getNbColumns() {return nbColumns.get();}
        public void setNbColumns(int nbColumns) {this.nbColumns.set(nbColumns);}
        public IntegerProperty nbColumnsProperty() {return nbColumns;}

    /**
     * Dictionnaire qui pour chaque coordonnées du plateau fait correspondre une piece encapsulé dans une map property
     */
    private final MapProperty<Coordinate, ObjectProperty<Piece>> dicoCoord = new SimpleMapProperty<>();
        public ObservableMap<Coordinate, ObjectProperty<Piece>> getDicoCoord() {return dicoCoord.get();}
        public void setDicoCoord(ObservableMap<Coordinate, ObjectProperty<Piece>> dicoCoord) {this.dicoCoord.set(dicoCoord);}
        public MapProperty<Coordinate, ObjectProperty<Piece>> dicoCoordProperty() {return dicoCoord;}

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public Board()
    {
        setNbRows(SIDE_LENGHT);
        setNbColumns(SIDE_LENGHT);
    }

    /**
     * Constructeurs 2 paramètres
     * @param nbRows nombre de ligne
     * @param nbColumns nombre de colonne
     */
    public Board(int nbRows, int nbColumns)
    {
        setNbRows(nbRows);
        setNbColumns(nbColumns);
        setDicoCoord(FXCollections.observableHashMap());
    }

    /**
     * @param nbRows nombre de ligne
     * @param nbColumns nombre de colonne
     * @param dicoCoord le dictionnaire des pieces
     */
    public Board(int nbRows, int nbColumns,ObservableMap<Coordinate, ObjectProperty<Piece>> dicoCoord){
        this(nbRows,nbColumns);
        setDicoCoord(dicoCoord);
    }

    /**
     * @param p piece a bouger
     * @param i déplacement
     * @return un indice de déplacement
     */
    public abstract int getMoveFromInt(Piece p, int i);
    public abstract List<String> getCoord();


}
