package model.business;

import java.util.List;

/**
 * Définit le fait qu'une piece puisse se déplacer
 */
public interface IMovable {
    List<Coordinate> getMoveFromBoard(Board board);
}
