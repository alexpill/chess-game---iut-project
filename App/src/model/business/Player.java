package model.business;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Objects;

/**
 * Classe metier des joueurs
 */
public class Player {

    /**
     * Nom du joueur encapsulé dans une string property
     */
    private final StringProperty username = new SimpleStringProperty();
        public String getUsername() { return username.get();}
        public void setUsername(String username) {this.username.set(username);}
        public StringProperty usernameProperty() {return username;}

    /**
     * constructeur permettant d'initialiser le nom du joueur
     * @param name nom du joueur
     */
    public Player(String name)
    {
        setUsername(name);
    }

    /**
     * Methode d'égalité entre un objet et un player
     * @param o objet a comparer
     * @return true si meme objet false sinon
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(getUsername(), player.getUsername());
    }

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public Player(){

    }

    @Override
    public String toString(){ return String.valueOf(username);}

    @Override
    public int hashCode() {
        return Objects.hash(getUsername());
    }
}
