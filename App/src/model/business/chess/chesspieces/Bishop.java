package model.business.chess.chesspieces;

import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.PieceColor;
import model.business.chess.ChessCoordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe du fou dans une jeu d'échecs
 */
public class Bishop extends Piece {
    /**
     * Nom de la piece
     */
    private static final String PIECE_NAME = "Bishop";

    /**
     * Image d'un fou blanc
     */
    private static final String IMAGE_WHITE = "/images/chesspieces/wb.png";

    /**
     * Image d'un fou noir
     */
    private static final String IMAGE_BLACK = "/images/chesspieces/bb.png";

    /**
     * Liste des déplacements possibles selon la méthode mail Box
     */
    private static final List<Integer> MOVE = List.of(-11, 11, -9, 9);

    /**
     * @param c couleur de la piece
     */
    public Bishop(PieceColor c) {
        super(PIECE_NAME, c);
    }

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public Bishop(){}

    /**
     * @param c couleur de la piece
     * @param coord coordonnée de la piece
     */
    public Bishop(PieceColor c, Coordinate coord) {
        super(PIECE_NAME,c, coord);
        switch (c) {
            case WHITE:
//                setImage(new Image(IMAGE_WHITE));
                setImageURL(IMAGE_WHITE);
                break;

            case BLACK:
//                setImage(new Image(IMAGE_BLACK));
                setImageURL(IMAGE_BLACK);
                break;
        }
    }

    /**
     * permet d'avoir une liste des coordonnées de déplacements possible depuis un Board
     * @param board plateau d'où veut se déplacer la piece
     * @return
     */
    @Override
    public List<Coordinate> getMoveFromBoard(Board board) {
        List<Coordinate> listCord = new ArrayList<>();
        Coordinate cc;
        for( Integer i : MOVE) {
            boolean locker = false;
            int cpt = 1;
            do{
                int valueInTab120 = board.getMoveFromInt(this,i*cpt);
                if(valueInTab120 == -1 || locker){
                    cc = null;
                }
                else{
                    try {
                        cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                        if(board.getDicoCoord().get(cc).get() != null){
                            if(board.getDicoCoord().get(cc).get().getPieceColor() != this.getPieceColor()){
                                locker = true;
                            }
                            else{
                                cc = null;
                            }
                        }
                    } catch (Exception e) {
                        cc = null;
                    }
                }
                if(cc != null){
                    listCord.add(cc);
                }
                cpt++;
            }while(cc != null);
        }
        return listCord;
    }
}
