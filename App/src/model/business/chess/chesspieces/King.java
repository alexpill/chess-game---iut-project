package model.business.chess.chesspieces;

import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.PieceColor;
import model.business.chess.ChessCoordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe du roi dans un jeu d'échec
 */
public class King extends Piece {

    /**
     * Nom de la piece
     */
    private static final String PIECE_NAME = "King";

    /**
     * image d'un roi blanc
     */
    private static final String IMAGE_WHITE = "/images/chesspieces/wk.png";

    /**
     * Image d'un roi blanc
     */
    private static final String IMAGE_BLACK = "/images/chesspieces/bk.png";

    /**
     * Liste des déplacements possibles selon la méthode mailBox
     */
    private final List<Integer> MOVE = List.of( -10, 10, -1, 1, -11, 11, -9, 9 );

    /**
     * @param c couleur de la piece
     * @param coord coordonéé de la piece
     */
    public King(PieceColor c, Coordinate coord)
    {
        super(PIECE_NAME,c, coord);
        switch (c) {
            case WHITE:
//                setImage(new Image(IMAGE_WHITE));
                setImageURL(IMAGE_WHITE);
                break;

            case BLACK:
//                setImage(new Image(IMAGE_BLACK));
                setImageURL(IMAGE_BLACK);
                break;
        }
    }

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public King(){}

    /**
     * @param c couleur de la piece
     */
    public King(PieceColor c)
    {
        super(PIECE_NAME,c);
        switch (c)
        {
            case WHITE:
//                setImage(new Image(IMAGE_WHITE));
                setImageURL(IMAGE_WHITE);
                this.setCoordinate(new ChessCoordinate(1,"e"));
                break;

            case BLACK:
//                setImage(new Image(IMAGE_BLACK));
                setImageURL(IMAGE_BLACK);
                this.setCoordinate(new ChessCoordinate(8,"e"));
                break;
        }
    }

    /**
     * Permet de retourner une liste des coordonnées de déplacements possible
     * @param board plateau d'où veut se déplacer la piece
     * @return
     */
    @Override
    public List<Coordinate> getMoveFromBoard(Board board) {
        List<Coordinate> listCord = new ArrayList<>();
        Coordinate cc;
        for( Integer i : MOVE) {
            boolean locker = false;
            int valueInTab120 = board.getMoveFromInt(this,i);
            if(valueInTab120 == -1){
                cc = null;
            }
            else{
                try {
                    cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                    if(board.getDicoCoord().get(cc).get() != null){
                        if(board.getDicoCoord().get(cc).get().getPieceColor() != this.getPieceColor()){
                            locker = true;
                        }
                        else{
                            cc = null;
                        }
                    }
                } catch (Exception e) {
                    cc = null;
                }
            }
            if(cc != null){
                listCord.add(cc);
            }
        }
        return listCord;
    }

}
