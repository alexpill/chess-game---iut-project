package model.business.chess.chesspieces;

import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.PieceColor;
import model.business.chess.ChessCoordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de la Reine au échecs
 */
public class Queen extends Piece {

    /**
     * Nom de la piece
     */
    private static final String PIECE_NAME = "Queen";

    /**
     * image de la reine blanche
     */
    public static final String IMAGE_WHITE = "/images/chesspieces/wq.png";

    /**
     * image de la reine noire
     */
    public static final String IMAGE_BLACK = "/images/chesspieces/bq.png";

    /**
     * Définit les déplacements possibles selon la méthode mailBox
     */
    private static final List<Integer> move = List.of( -10, 10, -1, 1, -11, 11, -9, 9 );

    /**
     * Constructeur 2 paramètres
     * @param c couleur de la piece
     * @param coord coordonées de la piece
     */
    public Queen(PieceColor c, Coordinate coord)
    {
        super(PIECE_NAME,c, coord);
        switch (c) {
            case WHITE:
//                setImage(new Image(IMAGE_WHITE));
                setImageURL(IMAGE_WHITE);
                break;

            case BLACK:
//                setImage(new Image(IMAGE_BLACK));
                setImageURL(IMAGE_BLACK);
                break;
        }
    }

    /**
     * Constructeur vide pour le sérialisation XML
     */
    public Queen(){}

    /**
     * Constructeur 1 paramètre
     * @param c couleur de la piece
     */
    public Queen(PieceColor c)
    {
        super(PIECE_NAME,c);
        switch (c)
        {
            case WHITE:
//                setImage(new Image(IMAGE_WHITE));
                setImageURL(IMAGE_WHITE);
                this.setCoordinate(new ChessCoordinate(1,"d"));
                break;

            case BLACK:
//                setImage(new Image(IMAGE_BLACK));
                setImageURL(IMAGE_BLACK);
                this.setCoordinate(new ChessCoordinate(8,"d"));
                break;
        }
    }


    /**
     * Permet d'avoir une liste de coordonées de  déplacement possible
     * @param board plateau d'où veut se déplacer la piece
     * @return
     */
    @Override
    public List<Coordinate> getMoveFromBoard(Board board) {
        List<Coordinate> listCord = new ArrayList<>();
        Coordinate cc;
        for( Integer i : move) {
            boolean locker = false;
            int cpt = 1;
            do{
                int valueInTab120 = board.getMoveFromInt(this,i*cpt);
                if(valueInTab120 == -1 || locker){
                    cc = null;
                }
                else{
                    try {
                        cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                        if(board.getDicoCoord().get(cc).get() != null){
                            if(board.getDicoCoord().get(cc).get().getPieceColor() != this.getPieceColor()){
                                locker = true;
                            }
                            else{
                                cc = null;
                            }
                        }
                    } catch (Exception e) {
                        cc = null;
                    }
                }
                if(cc != null){
                    listCord.add(cc);
                }
                cpt++;
            }while(cc != null);
        }
        return listCord;
    }
}
