package model.business.chess.chesspieces;

import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.PieceColor;
import model.business.chess.ChessCoordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Défini la tour au échecs
 */
public class Rook extends Piece {

    /**
     * Nom de la piece
     */
    private static final String PIECE_NAME = "Rook";

    /**
     * image de la piece blanche
     */
    public static final String IMAGE_WHITE = "/images/chesspieces/wr.png";

    /**
     * Image de la piece noire
     */
    public static final String IMAGE_BLACK = "/images/chesspieces/br.png";

    //Compared to tab120 and tab64, moving a case: {top, bottom, left, right}
    /**
     * Déplacements possibles selon la méthode mail box
     */
    private static final List<Integer> MOVE = List.of(-10, 10, -1, 1) ;

    /**
     * Constructeur 1 paramètre
     * @param c couleur de la piece
     */
    public Rook(PieceColor c) {
        super(PIECE_NAME, c);
    }

    /**
     * Constructeur 2 paramètres
     * @param c couleur de la piece
     * @param coord coordonnée de la piece
     */
    public Rook(PieceColor c, Coordinate coord)
    {
        super(PIECE_NAME,c, coord/*,mv*/);
        switch (c) {
            case WHITE:
//                setImage(new Image(IMAGE_WHITE));
                setImageURL(IMAGE_WHITE);
                break;

            case BLACK:
//                setImage(new Image(IMAGE_BLACK));
                setImageURL(IMAGE_BLACK);
                break;
        }
    }

    /**
     * Constructeur vide pour la serialisation XML
     */
    public Rook(){}

    /**
     * Renvoie la liste des coordonées ou peut se déplacer la piece
     * @param board plateau d'où veut se déplacer la piece
     * @return
     */
    @Override
    public List<Coordinate> getMoveFromBoard(Board board) {
        List<Coordinate> listCord = new ArrayList<>();
        Coordinate cc;
        for( Integer i : MOVE) {
            boolean locker = false;
            int cpt = 1;
            do{
                int valueInTab120 = board.getMoveFromInt(this,i*cpt);
                if(valueInTab120 == -1 || locker){
                    cc = null;
                }
                else{
                    try {
                        cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                        if(board.getDicoCoord().get(cc).get() != null){
                            if(board.getDicoCoord().get(cc).get().getPieceColor() != this.getPieceColor()){
                                locker = true;
                            }
                            else{
                                cc = null;
                            }
                        }
                    } catch (Exception e) {
                        cc = null;
                    }
                }
                if(cc != null){
                    listCord.add(cc);
                }
                cpt++;
            }while(cc != null);
        }
        return listCord;
    }
}
