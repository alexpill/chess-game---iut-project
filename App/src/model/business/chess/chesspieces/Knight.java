package model.business.chess.chesspieces;

import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.PieceColor;
import model.business.chess.ChessCoordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe des Chevaliers dans un jeu d'échec
 */
public class Knight extends Piece {

    /**
     * Nom de la piece
     */
    private static final String PIECE_NAME = "Knight";

    /**
     * image d'un chevalier blanc
     */
    private static final String IMAGE_WHITE = "/images/chesspieces/wn.png";

    /**
     * image d'un chevalier noir
     */
    private static final String IMAGE_BLACK = "/images/chesspieces/bn.png";

    /**
     * Liste des mouvemetns possibles selon la méthode MailBox
     */
    private static final List<Integer> MOVE = List.of( -12, -21, -19, -8, 12, 21, 19, 8);

    /**
     * @param c couleur de la piece
     */
    public Knight(PieceColor c) {
        super(PIECE_NAME,c );
    }

    /**
     * @param c couleur de la piece
     * @param coord coordonnée de la piece
     */
    public Knight(PieceColor c, Coordinate coord)
    {
        super(PIECE_NAME,c, coord);
        switch (c) {
            case WHITE:
                setImageURL(IMAGE_WHITE);
                break;

            case BLACK:
                setImageURL(IMAGE_BLACK);
                break;
        }
    }

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public Knight(){}

    /**
     * Permet de retourner une liste des coordonées de déplacements possible
     * @param board plateau d'où veut se déplacer la piece
     * @return
     */
    @Override
    public List<Coordinate> getMoveFromBoard(Board board) {
        List<Coordinate> listCord = new ArrayList<>();
        Coordinate cc;
        for( Integer i : MOVE) {
            boolean locker = false;
            int valueInTab120 = board.getMoveFromInt(this,i);
            if(valueInTab120 == -1){
                cc = null;
            }
            else{
                try {
                    cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                    if(board.getDicoCoord().get(cc).get() != null){
                        if(board.getDicoCoord().get(cc).get().getPieceColor() != this.getPieceColor()){
                            locker = true;
                        }
                        else{
                            cc = null;
                        }
                    }
                } catch (Exception e) {
                    cc = null;
                }
            }
            if(cc != null){
                listCord.add(cc);
            }
        }
        return listCord;
    }
}
