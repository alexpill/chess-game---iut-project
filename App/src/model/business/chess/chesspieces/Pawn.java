package model.business.chess.chesspieces;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.PieceColor;
import model.business.chess.ChessCoordinate;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

/**
 * Classe d'un pion au échecs
 */
public class Pawn extends Piece {

    /**
     * Nom de la piece
     */
    private static final String PIECE_NAME = "Pawn";

    /**
     * image d'un pion blanc
     */
    private static final String IMAGE_WHITE = "/images/chesspieces/wp.png";

    /**
     * image d'un pion noir
     */
    private static final String IMAGE_BLACK = "/images/chesspieces/bp.png";

    /**
     * Liste des déplacements possibles selon la methode MailBox
     */
    private static final List<Integer> MOVE = List.of( -10, -9, -11 );

    /**
     * Définit si un pion a déja bougé ou non encapsulé dans une Boolean property
     */
    private final BooleanProperty firstMove = new SimpleBooleanProperty(true);
        public boolean isFirstMove() {return firstMove.get();}
        public void setFirstMove(boolean firstMove) {this.firstMove.set(firstMove);}
        public BooleanProperty firstMoveProperty() {return firstMove;}

    /**
     * Constructeur 1 paramètre
     * @param c couleur de la piece
     */
    public Pawn(PieceColor c) {
        super(PIECE_NAME, c);
    }

    /**
     * Constructeur 2 paramètres
     * @param c couleur de la piece
     * @param coord coordonnée de la piece
     */
    public Pawn(PieceColor c, Coordinate coord)
    {
        super(PIECE_NAME,c, coord);
        switch (c) {
            case WHITE:
//                setImage(new Image(IMAGE_WHITE));
                setImageURL(IMAGE_WHITE);
                break;

            case BLACK:
//                setImage(new Image(IMAGE_BLACK));
                setImageURL(IMAGE_BLACK);
                break;
        }
    }

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public Pawn(){}

    /**
     * Permet d'avoir une liste de coordonnées des déplacements possible
     * @param board plateau d'où veut se déplacer la piece
     * @return
     */
    @Override
    public List<Coordinate> getMoveFromBoard(Board board) {
        List<Coordinate> listCord = new ArrayList<>();
        Coordinate cc;
        for( Integer i : MOVE) {
            if(this.getPieceColor() == PieceColor.BLACK) { i = -i; }
            boolean locker = false;
            try {
                if(abs(i) == 10){
                    if(isFirstMove()){
                        int valueInTab120 = board.getMoveFromInt(this,i*2);
                        cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                        listCord.add(cc);
                    }
                    int valueInTab120 = board.getMoveFromInt(this,i);
                    if(valueInTab120 == -1){
                        cc = null;
                    }
                    else{
                        cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                        if(board.getDicoCoord().get(cc).get() != null){
                            cc = null;
                        }
                    }


                }
                else{
                    int valueInTab120 = board.getMoveFromInt(this,i);
                    if(valueInTab120 == -1){
                        cc = null;
                    }
                    else{
                        cc = ChessCoordinate.coordFromString(board.getCoord().get(valueInTab120));
                        if(board.getDicoCoord().get(cc).get() == null || board.getDicoCoord().get(cc).get().getPieceColor() == this.getPieceColor() ){
                            cc = null;
                        }
                    }
                }
            } catch (Exception e) {
                cc = null;
            }

            if(cc != null){
                listCord.add(cc);
            }
        }
        return listCord;
    }
}
