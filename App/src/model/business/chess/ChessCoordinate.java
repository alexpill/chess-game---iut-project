package model.business.chess;

import model.business.Coordinate;

import java.util.List;

import static java.lang.Math.abs;

/**
 * Classe concrète de coordonnée d'échecs
 */
public class ChessCoordinate extends Coordinate {
    /**
     * Défini le nombre maximal de ligne
     */
    public static final int ROW_MAX = 8;

    /**
     * Définit le nombre minimal de ligne
     */
    public static final int ROW_MIN = 1;

    /**
     * Liste des colonnes disponnibles
     */
    public static final List<String> listColumn = List.of(
            "a","b","c","d","e","f","g","h");

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public ChessCoordinate(){}

    /**
     * Constructeur 2 paramètres
     * @param row indice de la ligne
     * @param column indicde de la colonne
     */
    public ChessCoordinate(int row, String column)
    {
        super(row,column);
    }

    @Override
    public void setColumn(String columns) {
        if(listColumn.contains(columns))
        {
            super.setColumn(columns);
        }
    }

    @Override
    public void setRow(int row)
    {
        if(row <= ROW_MAX && row >= ROW_MIN)
        {
            super.setRow(row);
        }
    }

    /**
     * Convertit un string en coordonée
     * @param string le string a convertir
     * @return une coordonnée
     * @throws Exception renvoyé si le string n'est pas correcte
     */
    public static Coordinate coordFromString(String string) throws Exception {
            String column  = string.substring(0,1);
            if(!listColumn.contains(column)){ throw new Exception("Coordonnée invalide");}
            String row  = string.substring(1);
            int rowInt = Integer.parseInt(row);
            if(rowInt < ROW_MIN || rowInt > ROW_MAX){ throw new Exception("Coordonnée invalide");}
            return new ChessCoordinate(rowInt,column);
    }
}
