package model.business;

import model.business.Piece;
import model.business.Player;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import java.util.List;

/**
 * Cimetière d'un joueur
 */
public class Graveyard {


    /**
     * contient les liste de piece dans le cimetiètre en fonction du joueur
     */
    private ObservableMap<Player, ObservableList<Piece>> dicoGraveyard = FXCollections.observableHashMap();
    public ObservableMap<Player, ObservableList<Piece>> getDicoGraveyard() {return dicoGraveyard;}
    public void setDicoGraveyard(ObservableMap<Player, ObservableList<Piece>> dicoGraveyard) {this.dicoGraveyard = dicoGraveyard;}


    /**
     * constructeur à 1 paramètre
     * @param lp liste des joueur
     */
    public Graveyard(List<Player> lp){
        for (Player p : lp) {
            dicoGraveyard.put(p, FXCollections.observableArrayList());
        }

    }

    /**
     * Constructeur vide pour la sérialisation XML
     */
    public Graveyard(){}

    public Graveyard(ObservableMap<Player, ObservableList<Piece>> dicoGraveyard){
        setDicoGraveyard(dicoGraveyard);
    }

    /**
     * Ajoute une piece dans le cimetière d'un joueur
     * @param player joueur qui vient de perdre une piece
     * @param piece piece perdu
     */
    public void tookPiece(Player player, Piece piece){
        dicoGraveyard.get(player).add(piece);
    }


    /**
     * Permet de ressucité une piece
     * @param player joueur qui veut ressuciter une piece
     * @param piece piece a réssuciter
     */
    public void rebornPiece(Player player,Piece piece){
        dicoGraveyard.get(player).remove(piece);
    }

    @Override
    public String toString(){
        String text = "";
        for (ObservableMap.Entry<Player, ObservableList<Piece>> entry : dicoGraveyard.entrySet())
        {
            text+=entry.getKey().toString();
            text+=entry.getValue();
        }
        return text;
    }
}
