package model;

import model.business.HistoryElement;
import model.business.Piece;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Classe qui contient les données relatives à l'historique et ses informations
 */
public class HistoryModel {
    /**
     * La liste observable des éléements d'historiques
     */
    private ObservableList<HistoryElement> historyElementObservable = FXCollections.observableArrayList();

    /**
     * L'historique encapsulé dans une propriété pour le binding
     */
    private final ListProperty<HistoryElement> history = new SimpleListProperty<>(historyElementObservable);
        public ObservableList<HistoryElement> getHistory() {return history.get();}
        public ListProperty<HistoryElement> historyProperty() {return history;}
        public void setHistory(ObservableList<HistoryElement> history) {this.history.set(history);}

    /**
     * Constructeur par defaut vide pour la serialisation XML
     */
    public HistoryModel(){}

    /**
     * Permet d'ajouter un élément d'historique sans prise de piece
     * @param p piece a ajouter
     */
    public void addHistory(Piece p){
            if(p != null){
                getHistory().add(new HistoryElement(p));
            }
    }

    /**
     * Permet d'ajouter un élément d'historique avec prise de piece
     * @param p piece a ajouter
     * @param taken piece prise
     */
    public void addHistory(Piece p, Piece taken){
        if(p != null && taken != null){
            getHistory().add(new HistoryElement(p,taken));
        }
    }

    /**
     * Remet l'historique à zero
     */
    public void resetHistory(){
            getHistory().clear();
    }
}
