package model;

import model.business.Player;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Classe contenant les données relatives au player
 */
public class PlayerModel {
    /**
     * Joueur 1 encapsulé pour le binding
     */
    private final ObjectProperty<Player> p1 = new SimpleObjectProperty<>();
        public Player getP1() {return p1.get();}
        public void setP1(Player p1) {this.p1.set(p1);}
        public ObjectProperty<Player> p1Property() {return p1;}

    /**
     * Joueur 2 encapsulé pour le binding
     */
    private final ObjectProperty<Player> p2 = new SimpleObjectProperty<>();
    public Player getP2() {return p2.get();}
    public void setP2(Player p2) {this.p2.set(p2);}
    public ObjectProperty<Player> p2Property() {return p2;}

    /**
     * Jouer jouant actuellement encapsulé pour le binding
     */
    private final ObjectProperty<Player> readyPlayer = new SimpleObjectProperty<>();
    public Player getReadyPlayer() {return readyPlayer.get();}
    public void setReadyPlayer(Player readyPlayer) {this.readyPlayer.set(readyPlayer);}
    public ObjectProperty<Player> readyPlayerProperty() {return readyPlayer;}


    /**
     * Permet d'alterner le tour des joueurs
     */
    public void changePlayer(){
        setReadyPlayer((getReadyPlayer() == getP1()) ? getP2() : getP1());
    }

    /**
     * Constructeur vide pour la serialisation XML
     */
    public PlayerModel(){}

    /**
     * Constructeur à deux paramètres d'un PlayerModel
     * @param p1 player 1
     * @param p2 player 2
     */
    public PlayerModel(Player p1, Player p2)
    {
        setP1(p1);
        setP2(p2);
        setReadyPlayer(p1);

    }

}
