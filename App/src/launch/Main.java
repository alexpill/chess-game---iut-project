package launch;

import controller.GameMediator;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.stage.StageStyle;
import model.business.data.Manager;
import model.business.data.StubChessDataManager;
import model.business.data.XMLChessDataManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import view.viewcontrollers.StartViewControl;

public class Main extends Application {
    public static final String IMAGES_CHESSPIECES_WN_PNG = "/images/chesspieces/wn.png";
    public static final String FXML_START_FXML = "/fxml/start.fxml";
    public static final String STYLESHEETS_START_CSS = "/stylesheets/start.css";

//    public static final String WINDOW_TITLE = "Board games";

    private double xOffset = 0;
    private double yOffset = 0;

    private Manager manager;

    private void loadAll(){
        manager = new Manager(new XMLChessDataManager());
        if(manager.getBoard() == null || manager.getHistoryModel() == null || manager.getPlayerModel() == null || manager.getGraveyard() == null) {
            manager.setDataManager(new StubChessDataManager());
            manager.load();
        }
    }

    private void saveAll(){
        manager.setDataManager(new XMLChessDataManager());
        manager.save();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{


        primaryStage.initStyle(StageStyle.TRANSPARENT);
        //stage.initStyle(StageStyle.UNDERDECORATED);


        loadAll();
        GameMediator gameMediator = new GameMediator(manager);

        primaryStage.getIcons().add(new Image(IMAGES_CHESSPIECES_WN_PNG));
        FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_START_FXML));
        StartViewControl cc = new StartViewControl(gameMediator);
        loader.setController(cc);

        Parent root = loader.load();

        cc.top_bar.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });

        cc.top_bar.setOnMouseDragged(event -> {
            primaryStage.setX(event.getScreenX() - xOffset);
            primaryStage.setY(event.getScreenY() - yOffset);
        });

        cc.setGameMediator(gameMediator);

//        primaryStage.setTitle(WINDOW_TITLE);
        StartViewControl controller = loader.getController();
        Scene scn = new Scene(root);
        scn.getStylesheets().add(getClass().getResource(STYLESHEETS_START_CSS).toExternalForm());
        primaryStage.setScene(scn);
        primaryStage.show();
        controller.setGameMediator(gameMediator);
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() {
        saveAll();
    }
}
