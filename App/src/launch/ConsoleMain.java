package launch;

import model.business.Board;
import model.business.Coordinate;
import model.business.Piece;
import model.business.data.BoardFactory;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.stage.Stage;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static javafx.application.Platform.exit;

public class ConsoleMain extends Application {

    public static void main(String[] args) {
        BoardFactory bf = new BoardFactory();
        Board b = bf.chessBoardFactory();
        /*IDataManager dt = new XMLChessDataManager();
        dt.saveBoard(b);
        b = dt.loadBoard();
        System.out.println(b.getNbRows());
        System.out.println(b.getNbRows());
        System.out.println(b.getDicoCoord());*/
        try (XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("board.xml")))) {
            Map<Coordinate, ObjectProperty<Piece>> dicoCoord = new HashMap<>(b.getDicoCoord());
            encoder.writeObject(dicoCoord);
            encoder.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
        exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
