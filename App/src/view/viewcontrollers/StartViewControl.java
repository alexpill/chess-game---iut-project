package view.viewcontrollers;

import controller.GameMediator;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import launch.Main;

import java.io.IOException;

import static view.viewcontrollers.ChessViewController.LIST_IMAGE_SIZE;

/**
 * Controlleur de la vue du choix des noms de personnages
 */
public class StartViewControl {

    private static final String FXML_CHESS_FXML = "/fxml/chess.fxml";
    private static final String STYLESHEETS_CHESS_CSS = "/stylesheets/chess.css";
    private static final String ICON = "/images/chesspieces/wn.png";
    /**
     * Permet de gérer le déplacement de la fenêtre
     */
    private double xOffset = 0;

    /**
     * Permet de gérer le déplacement de la fenêtre
     */
    private double yOffset = 0;

    /**
     * Game médiator passé depuis le main
     */
    private final ObjectProperty<GameMediator> gameMediator = new SimpleObjectProperty<>(null);
        private GameMediator getGameMediator() {return gameMediator.get();}
        public void setGameMediator(GameMediator gameMediator) {this.gameMediator.set(gameMediator);}
        public ObjectProperty<GameMediator> gameMediatorProperty() {return gameMediator;}

    /**
     * Champs de sélection du nom du player 2 dans la vue
     */
    @FXML
    private TextField P2Text;

    /**
     * Champs de sélection du nom du player 1 dans la vue
     */
    @FXML
    private TextField P1Text;

    /**
     * Affichage dans la vue du nom du player 1
     */
    @FXML
    private Label Label1;

    /**
     * Affichage dans la vue du nom du player 2
     */
    @FXML
    private Label Label2;

    /**
     * Button pour lancer la partie
     */
    @FXML
    private Button buttonStart;

    /**
     * Bouton pour fermer la fenêtre
     */
    @FXML
    private Button closeButton;

    /**
     * Icone de la fenetre
     */
    @FXML
    private ImageView icon;

    /**
     * Barre en haut de fenetre
     */
    @FXML
    public HBox top_bar;

    @FXML private Label label_name_1;
    @FXML private Label label_name_2;
    @FXML private Label title;

    /**
     * Permet d'initialiser la vue
     */
    @FXML
    private void initialize() {

        icon.setPreserveRatio(true);
        icon.setFitWidth(LIST_IMAGE_SIZE);
        icon.setImage(new Image(ICON));
        buttonStart.setText(internationalization.I18n.getString("Start"));
        label_name_1.setText(internationalization.I18n.getString("NamePlayer1"));
        label_name_2.setText(internationalization.I18n.getString("NamePlayer2"));
        title.setText(internationalization.I18n.getString("Title"));
        P1Text.textProperty().bindBidirectional(getGameMediator().getManager().getPlayerModel().getP1().usernameProperty());
        P2Text.textProperty().bindBidirectional(getGameMediator().getManager().getPlayerModel().getP2().usernameProperty());
        Label1.textProperty().bind(getGameMediator().getManager().getPlayerModel().getP1().usernameProperty());
        Label2.textProperty().bind(getGameMediator().getManager().getPlayerModel().getP2().usernameProperty());
        buttonStart.setDefaultButton(true);

    }

    /**
     * Constructeur d'un StartViewControl
     * @param gm : le game médiateur a utliser
     */
    public StartViewControl(GameMediator gm){
        setGameMediator(gm);
    }

    /**
     * Gestion de l'évènement de vlie sur le bouton de lancement
     * @param actionEvent contient les information sur le click
     * @throws IOException gère les erreur de fichiers
     */
    @FXML
    public void buttonClick(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage)((Button)actionEvent.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_CHESS_FXML));

        ChessViewController cc = new ChessViewController(getGameMediator());

        getGameMediator().setViewController(cc);
        loader.setController(cc);

        Parent root = loader.load();

        cc.top_bar.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });

        cc.top_bar.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - xOffset);
            stage.setY(event.getScreenY() - yOffset);
        });


        cc.pl_name1.textProperty().bind(getGameMediator().getManager().getPlayerModel().getP1().usernameProperty());
        cc.pl_name2.textProperty().bind(getGameMediator().getManager().getPlayerModel().getP2().usernameProperty());
        Scene scn = new Scene(root);
        scn.getStylesheets().add(getClass().getResource(STYLESHEETS_CHESS_CSS).toExternalForm());
        stage.setScene(scn);
        stage.setResizable(false); // for the moment
        stage.show();
    }

    /**
     * Permet de fermer la fenetre si on clique sur le bouton quit
     * @param actionEvent contient les information sur le click
     */
    public void close(ActionEvent actionEvent) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
