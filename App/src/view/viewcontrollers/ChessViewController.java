package view.viewcontrollers;

import internationalization.I18n;
import controller.GameMediator;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.business.Coordinate;
import model.business.HistoryElement;
import model.business.Piece;
import model.business.PieceColor;
import model.business.chess.ChessCoordinate;
import model.business.chess.chesspieces.King;
import model.business.chess.chesspieces.Pawn;
import utils.BindingUtils;
import view.ChoiceGraveyard;
import view.CoordinateToMoveConverter;

import java.util.Optional;


/**
 * Controller de la vue de l'échiquer
 */
public class ChessViewController {

    /**
     * Icone de la page
     */
    private static final String ICON = "/images/chesspieces/wn.png";

    //TODO string pas imuable pour le moment
    /**
     * Game médiateur de la vue
     */
    private final ObjectProperty<GameMediator> gameMediator = new SimpleObjectProperty<>(null);
        public GameMediator getGameMediator() {return gameMediator.get();}
        public void setGameMediator(GameMediator gameManager) {this.gameMediator.set(gameManager);}
        public ObjectProperty<GameMediator> gameMediatorProperty() {return gameMediator;}

    /**
     * taille des case de du board
     */
    public static final int CASE_SIZE = 80;

    /**
     * Taille des images dans la list le l'historique
     */
    static final int LIST_IMAGE_SIZE = 25;


    /**
     * Index pour le rectangle d'overlay dans la vue
     */
    public static final int INDEX_OVERLAY = 1;

    /**
     * Index pour l'image des pieces dans la vue
     */
    public static final int INDEX_IMAGE = 2;

    /**
     * taille des images dans la liste des cimetières
     */
    public static final int GRAVEYARD_IMAGE_SIZE = 15;

    /**
     * Couleur des cases supposées blanches
     */
    public static final Color FILL_WHITE = Color.WHITE;

    /**
     * Coulers des cases supposées noires
     */
    public static final Color FILL_BLACK = Color.DARKSLATEGRAY;

    /**
     * Couleur transparente
     */
    public static final Color FILL_TRANSPARENT = Color.TRANSPARENT;

    /**
     * Couleur de l'overlay
     */
    public static final Color FILL_OVERLAY = Color.BLUE;

    /**
     * Constructeur à 1 paramètre de ChessViewController
     * @param gameManager le manager a utiliser
     */
    public ChessViewController(GameMediator gameManager){
        setGameMediator(gameManager);
    }

    /**
     * Listener sur les pieces de chaque cases
     */
    public ChangeListener<Piece> pieceListener = (observableValue, oldPiece, newPiece) -> {
        Coordinate c = (oldPiece == null) ? newPiece.getCoordinate() : oldPiece.getCoordinate();
        int column = ChessCoordinate.listColumn.indexOf(c.getColumn());
        int row = c.getRow();
        if (newPiece == null) {
            getStackPane(column, row).getChildren().set(INDEX_IMAGE, new Rectangle(CASE_SIZE, CASE_SIZE, FILL_TRANSPARENT));
        } else {
            ImageView image = new ImageView(new Image(newPiece.getImageURL()));
            image.setPreserveRatio(true);
            image.setFitWidth(CASE_SIZE);
            getStackPane(column, row).getChildren().set(INDEX_IMAGE, image);
        }

    };
    public ChangeListener<Piece> getPieceListener() {
        return pieceListener;
    }

    /**
     * Listener sur le clique d'un élément du cimetières du player 1
     */
    private ChangeListener<Piece> graveyardListenerP1 = (observableValue, oldPiece, newPiece) -> {
        this.selectedPieceP1.setText((newPiece == null) ? "" : I18n.getString(newPiece.getName()));
    };

    /**
     * Listener sur le clique d'un élément du cimetières du player 2
     */
    private ChangeListener<Piece> graveyardListenerP2 = (observableValue, oldPiece, newPiece) -> {
        this.selectedPieceP2.setText((newPiece == null) ? "" : I18n.getString(newPiece.getName()));
    };

    @FXML private Label title;
    @FXML private Button reset;

    /**
     * VBox qui contient le board
     */
    @FXML public VBox board;
    public VBox getBoard() {
        return board;
    }

    /**
     * List View de l'historique des actions
     */
    @FXML public ListView<HistoryElement> historyList;

    /**
     * Label contenant le nom du player 2
     */
    @FXML Label pl_name2;

    /**
     * Label contenant le nom du player 1
     */
    @FXML Label pl_name1;

    /**
     * ListView du cimetière du player 1 dans la vue
     */
    @FXML public ListView<Piece> list_p1;

    /**
     * ListView du cimetière du player 2 dans la vue
     */
    @FXML public ListView<Piece> list_p2;

    /**
     * Text de la piece su cimetière sélectionnée du player 2
     */
    @FXML private Label selectedPieceP2;

    /**
     * Text de la piece su cimetière sélectionnée du player 1
     */
    @FXML private Label selectedPieceP1;

    /**
     * Bouton de fermeture de la fenetre
     */
    @FXML private Button closeButton;


    /**
     * Barre du haut de la fenetre
     */
    @FXML HBox top_bar;

    /**
     * Icone de la fenetre
     */
    @FXML private ImageView icon;

    /**
     * Permet d'initialiser les composant apres le constructeur
     */
    @FXML
    private void initialize() {

        icon.setPreserveRatio(true);
        icon.setFitWidth(LIST_IMAGE_SIZE);
        icon.setImage(new Image(ICON));
        title.setText(I18n.getString("Title"));
        reset.setText(I18n.getString("Reset"));

        getGameMediator().initBind();

        for (int i = 0; i < 4; i++) {
            HBox h1 = new HBox();
            initH1(h1);
            HBox h2 = new HBox();
            initH2(h2);
            board.getChildren().add(h1);
            board.getChildren().add(h2);
        }

        getGameMediator().setupImage();

        EventHandler<MouseEvent> ev = getGameMediator()::caseClick;

        for (Node item : board.getChildren()) {
            for (Node it2 : ((HBox) item).getChildren()) {
                it2.setOnMouseClicked(ev);

            }
        }
        historyList.setCellFactory(param -> cellFactoryInit());

        list_p1.getSelectionModel().selectedItemProperty().addListener(graveyardListenerP1);
        list_p2.getSelectionModel().selectedItemProperty().addListener(graveyardListenerP2);

    }

    /**
     * permet d'initialiser une Hbox avec une alternance de rectancle
     * @param h2 Hbox a initialiser
     */
    private void initH2(HBox h2) {
        for (int j = 0; j < 4; j++) {
            h2.getChildren().add(new StackPane(new Rectangle(CASE_SIZE, CASE_SIZE, FILL_BLACK),new Rectangle(CASE_SIZE,CASE_SIZE, FILL_TRANSPARENT)));
            h2.getChildren().add(new StackPane(new Rectangle(CASE_SIZE, CASE_SIZE, FILL_WHITE),new Rectangle(CASE_SIZE,CASE_SIZE, FILL_TRANSPARENT)));
        }
    }

    /**
     * Voir initH2 pareil mais décalage dans l'aleternance
     * @param h1 Hbox a initialiser
     */
    private void initH1(HBox h1) {
        for (int j = 0; j < 4; j++) {
            h1.getChildren().add(new StackPane(new Rectangle(CASE_SIZE, CASE_SIZE, FILL_WHITE),new Rectangle(CASE_SIZE,CASE_SIZE, FILL_TRANSPARENT)));
            h1.getChildren().add(new StackPane(new Rectangle(CASE_SIZE, CASE_SIZE, FILL_BLACK),new Rectangle(CASE_SIZE,CASE_SIZE, FILL_TRANSPARENT)));
        }
    }

    /**
     * Permet de formater les cell d'une ListView ici celle des cimetières
     * @return une cell
     */
    public ListCell<Piece> graveyardCellFact(){
        return new ListCell<>(){
            private ImageView image = null;
            @Override
            protected void updateItem(Piece item, boolean empty){
                super.updateItem(item, empty);
                if(empty){
                    setText("");
                    setGraphic(null);
                }else{
                    image= new ImageView(item.getImageURL());
                    image.setPreserveRatio(true);
                    image.setFitWidth(GRAVEYARD_IMAGE_SIZE);
                    setGraphic(image);
                }
            }

        };
    }


    /**
     * Permet de formater les cell d'une ListView ici celle de l'historique
     * @return une cell
     */
    private ListCell<HistoryElement> cellFactoryInit() {
        return new ListCell<>() {

            private HBox hBox = null;
            private Text actionField = null;
            private Text coordField = null;
            private ImageView imagePiece = null;
            private ImageView imageTaken = null;

            @Override
            protected void updateItem(HistoryElement item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    hBox = new HBox();
                    imagePiece = new ImageView();
                    imagePiece.imageProperty().bindBidirectional(BindingUtils.imageProperty(item.getThePiece().getImageURL()));
                    imagePiece.setPreserveRatio(true);
                    imagePiece.setFitWidth(LIST_IMAGE_SIZE);
                    hBox.getChildren().add(imagePiece);
                    actionField = new Text();
                    if(item.getThePieceTaken() != null){
                        imageTaken = new ImageView();
                        imageTaken.imageProperty().bindBidirectional(BindingUtils.imageProperty(item.getThePiece().getImageURL()));
                        imageTaken.setPreserveRatio(true);
                        imageTaken.setFitWidth(LIST_IMAGE_SIZE);
                        actionField.setText(" " + I18n.getString("Took") + " " );
                        hBox.getChildren().add(actionField);
                        hBox.getChildren().add(imageTaken);
                    }else{
                        actionField.setText(" " + I18n.getString("Move")+ " " );
                        hBox.getChildren().add(actionField);
                    }
                    coordField = new Text();
                    coordField.textProperty().bindBidirectional(item.getThePiece().coordinateProperty(),new CoordinateToMoveConverter());
                    hBox.getChildren().add(coordField);
                    setGraphic(hBox);
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        };
    }

    /**
     * Permet de retourner un stackpane avec des coordonnées données.
     * @param column indice de la colonne
     * @param row indicde de la ligne
     * @return le stackpane correspondant indice en paramètre
     */
    public StackPane getStackPane(int column, int row) {
        return (StackPane) (((HBox) board.getChildren().get(7 - (row - 1))).getChildren().get(column));
    }


    /**
     * Permet de recommencer une partie
     * @param actionEvent contient les informations sur le bouton
     */
    public void resetState(ActionEvent actionEvent) {
        getGameMediator().reset();
    }

    /**
     * Permet de fermer la fenetre
     * @param actionEvent contient les infomations sur le bouton cliqué
     */
    public void close(ActionEvent actionEvent) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}


