package view;

import com.sun.javafx.scene.control.skin.resources.ControlResources;
import model.business.Piece;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Classe créée à partir d'une dialog pour gérer le cimetières.
 */
public class ChoiceGraveyard extends Dialog<Piece> {

    /**************************************************************************
     *
     * Fields
     *
     **************************************************************************/

    private final GridPane grid;
    private final Label label;
    private final ComboBox<Piece> comboBox;
    private ImageView image;



    /**************************************************************************
     *
     * Constructors
     *
     **************************************************************************/

    /**
     * Creates a default, empty instance of ChoiceGraveyard with no set items and a
     * null default choice. Users of this constructor will subsequently need to
     * call { getItems()} to specify which items to show to the user.
     */
    public ChoiceGraveyard() {
        this(null, (Piece[])null);
    }

    /**
     * Creates a new ChoiceGraveyard instance with the first argument specifying the
     * default choice that should be shown to the user, and all following arguments
     * considered a varargs array of all available choices for the user. It is
     * expected that the defaultChoice be one of the elements in the choices varargs
     * array. If this is not true, then defaultChoice will be set to null and the
     * dialog will show with the initial choice set to the first item in the list
     * of choices.
     *
     * @param defaultChoice The item to display as the pre-selected choice in the dialog.
     *        This item must be contained within the choices varargs array.
     * @param choices All possible choices to present to the user.
     */
    private ChoiceGraveyard(Piece defaultChoice, Piece... choices) {
        this(defaultChoice,
                choices == null ? Collections.emptyList() : Arrays.asList(choices));
    }

    /**
     * Creates a new ChoiceGraveyard instance with the first argument specifying the
     * default choice that should be shown to the user, and the second argument
     * specifying a collection of all available choices for the user. It is
     * expected that the defaultChoice be one of the elements in the choices
     * collection. If this is not true, then defaultChoice will be set to null and the
     * dialog will show with the initial choice set to the first item in the list
     * of choices.
     *
     * @param defaultChoice The item to display as the pre-selected choice in the dialog.
     *        This item must be contained within the choices varargs array.
     * @param choices All possible choices to present to the user.
     */
    public ChoiceGraveyard(Piece defaultChoice, Collection<Piece> choices) {
        final DialogPane dialogPane = getDialogPane();

        // -- grid
        this.grid = new GridPane();
        this.grid.setHgap(10);
        this.grid.setMaxWidth(Double.MAX_VALUE);
        this.grid.setAlignment(Pos.CENTER_LEFT);

        // -- label
        label = createContentLabel(dialogPane.getContentText());
        label.setPrefWidth(Region.USE_COMPUTED_SIZE);
        label.textProperty().bind(dialogPane.contentTextProperty());

        dialogPane.contentTextProperty().addListener(o -> updateGrid());

        setTitle(ControlResources.getString("Dialog.confirm.title"));
        dialogPane.setHeaderText(ControlResources.getString("Dialog.confirm.header"));
        dialogPane.getStyleClass().add("choice-dialog");
        dialogPane.getButtonTypes().addAll(ButtonType.OK);

        final double MIN_WIDTH = 150;

        comboBox = new ComboBox<>();
        comboBox.setCellFactory(l -> new ListCell<>(){
                    @Override
                    protected void updateItem(Piece item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty){
                            setText("");
                            setGraphic(null);
                        }else{
                            image= new ImageView(item.getImageURL());
                            image.setPreserveRatio(true);
                            image.setFitWidth(50);
                            setText(item.toString());
                            setGraphic(image);
                        }
                    }
                }
        );
        comboBox.setMinWidth(MIN_WIDTH);
        if (choices != null) {
            comboBox.getItems().addAll(choices);
        }
        comboBox.setMaxWidth(Double.MAX_VALUE);
        GridPane.setHgrow(comboBox, Priority.ALWAYS);
        GridPane.setFillWidth(comboBox, true);


        if (defaultChoice == null) {
            comboBox.getSelectionModel().selectFirst();
        } else {
            comboBox.getSelectionModel().select(defaultChoice);
        }

        updateGrid();

        setResultConverter((dialogButton) -> {
            ButtonData data = dialogButton == null ? null : dialogButton.getButtonData();
            return data == ButtonData.OK_DONE ? getSelectedItem() : null;
        });
    }

    /**************************************************************************
     *
     * Static fields
     *
     **************************************************************************/

    /**
     * Creates a Label node that works well within a Dialog.
     * @param text The text to display
     */
    private static Label createContentLabel(String text) {
        Label label = new Label(text);
        label.setMaxWidth(Double.MAX_VALUE);
        label.setMaxHeight(Double.MAX_VALUE);
        label.getStyleClass().add("content");
        label.setWrapText(true);
        label.setPrefWidth(360);
        return label;
    }



    /**************************************************************************
     *
     * Public API
     *
     **************************************************************************/

    /**
     * Returns the currently selected item in the dialog.
     * @return the currently selected item
     */
    private  Piece getSelectedItem() {
        return comboBox.getSelectionModel().getSelectedItem();
    }





    /**************************************************************************
     *
     * Private Implementation
     *
     **************************************************************************/

    private void updateGrid() {
        grid.getChildren().clear();

        grid.add(label, 0, 0);
        grid.add(comboBox, 1, 0);
        getDialogPane().setContent(grid);

        Platform.runLater(() -> comboBox.requestFocus());
    }

}
