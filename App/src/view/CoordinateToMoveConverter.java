package view;


import internationalization.I18n;
import javafx.util.StringConverter;
import model.business.Coordinate;

/**
 * Converter pour tranformer une coordonnée en mouvement ( Coordinate = To + Coordinate)
 */
public class CoordinateToMoveConverter extends StringConverter<Coordinate> {

    /**
     * @param object La coordonées à convertir
     * @return la string du mouvement
     */
    @Override
    public String toString(Coordinate object) {
        StringBuilder builder = new StringBuilder();
        builder.append(I18n.getString("To"));
        builder.append(" ");
        builder.append(object.toString());
        return builder.toString();

    }

    /**
     * @param string le string a convertir en Coordinate
     * @return ici ne retourne rien
     */
    @Override
    public Coordinate fromString(String string) {
        return null;
    }
}
