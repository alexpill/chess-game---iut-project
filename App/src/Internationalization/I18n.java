package internationalization;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Classe de gestion des différent bundle
 */
public class I18n {

    /**
     * Définit la langue qui va être utilisée
     */
    private static Locale locale = Locale.getDefault();

    /**
     * Définit le ressource bundle
     */
    private static ResourceBundle resB = ResourceBundle.getBundle("view",locale);
//    public static ResourceBundle getResB() {return resB;}


    /**
     * @param t : String de la clé que l'on veut dans le ressource bundle
     * @return le string correspondant à la clé q'il existe
     */
    public static String getString(String t){
        return resB.getString(t);
    }

}
