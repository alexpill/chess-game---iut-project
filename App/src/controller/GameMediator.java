package controller;

import javafx.beans.property.SimpleListProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import model.business.*;
import model.business.chess.ChessCoordinate;
import model.business.chess.chesspieces.King;
import model.business.chess.chesspieces.Pawn;
import model.business.data.Manager;
import model.business.data.StubChessDataManager;
import model.business.data.XMLChessDataManager;
import view.ChoiceGraveyard;
import view.viewcontrollers.ChessViewController;

import java.util.*;

import static view.viewcontrollers.ChessViewController.*;

/**
 * Classe intégrant un patron Médiateur pour gérer les intéractions entre Vue et Données (Manager)
 */
public class GameMediator {

    /**
     * La vue que le Game Mediator gère
     */
    private ChessViewController viewController;
        private ChessViewController getViewController() {return viewController;}
        public void setViewController(ChessViewController viewController) {this.viewController = viewController;}

    /**
     * Le manager que le Game Mediator gère
     */
    private Manager manager;
        public Manager getManager() {return manager;}
        public void setManager(Manager manager) {this.manager = manager;}

    /**
     * Constructeur à 1 paramètre du GameMediator
     * @param manager : le Manager
     */
    public GameMediator(Manager manager) {
        this.manager = manager;
        viewController = null;
    }

    /**
     * Liste des coordonnées qui sont renvoyé lors d'une selection d'une case dans la vue
     */
    private List<Coordinate> lc = new ArrayList<>();

    /**
     * La pièce sélectionnée dans la vue
     */
    private Piece p = null;

    /**
     * Fonction permettant de mettre les pieces sur le plateau
     */
    private void setUpPieces() {
        for (Coordinate c : getManager().getBoard().getDicoCoord().keySet()) {
            int column = ChessCoordinate.listColumn.indexOf(c.getColumn());
            int row = c.getRow();
            ((HBox) getViewController().getBoard().getChildren().get(7 - (row - 1))).getChildren().get(column).setId(c.toString());
            getManager().getBoard().getDicoCoord().get(c).addListener(getViewController().getPieceListener());
            if (getManager().getBoard().getDicoCoord().get(c).get() == null)
            {
                getViewController().getStackPane(column, row).getChildren().set(ChessViewController.INDEX_IMAGE,new Rectangle(CASE_SIZE, CASE_SIZE, Color.TRANSPARENT));
                continue;
            }
            ImageView image = new ImageView(new Image(getManager().getBoard().getDicoCoord().get(c).get().getImageURL()));
            image.setPreserveRatio(true);
            image.setFitWidth(CASE_SIZE);
            getViewController().getStackPane(column, row).getChildren().set(ChessViewController.INDEX_IMAGE,image);
        }
    }

    /**
     * @return renvoie le Board du Manager
     */
    public Board getBoard(){
            return getManager().getBoard();
    }

    /**
     * Fonction permettant d'initialiser le binding entre la vue et le manager
     */
    public void initBind() {
        getViewController().list_p1.itemsProperty().bindBidirectional(new SimpleListProperty<>(getManager().getGraveyard().getDicoGraveyard().get(getManager().getPlayerModel().getP1())));
        getViewController().list_p1.setPrefHeight(ChessViewController.GRAVEYARD_IMAGE_SIZE + 10);
        getViewController().list_p1.setCellFactory(param -> getViewController().graveyardCellFact());
        getViewController().list_p2.itemsProperty().bindBidirectional(new SimpleListProperty<>(getManager().getGraveyard().getDicoGraveyard().get(getManager().getPlayerModel().getP2())));
        getViewController().list_p2.setPrefHeight(ChessViewController.GRAVEYARD_IMAGE_SIZE + 10);
        getViewController().list_p2.setCellFactory(param -> getViewController().graveyardCellFact());
        getViewController().historyList.itemsProperty().bindBidirectional(getManager().getHistoryModel().historyProperty());
    }

    /**
     * Fonction permettant de renouveller un plateau dans la vue + créer un état "initial" dans la manager
     */
    public void reset() {
        getManager().setDataManager(new StubChessDataManager());
        getManager().load();
        getViewController().historyList.itemsProperty().bindBidirectional(getManager().getHistoryModel().historyProperty());
        getManager().getHistoryModel().resetHistory();

        List<Player> lp = List.of(getManager().getPlayerModel().getP1(),getManager().getPlayerModel().getP2());
        getManager().setGraveyard(new Graveyard(lp));
        getViewController().list_p1.itemsProperty().bindBidirectional(new SimpleListProperty<>(getManager().getGraveyard().getDicoGraveyard().get(getManager().getPlayerModel().getP1())));
        getViewController().list_p2.itemsProperty().bindBidirectional(new SimpleListProperty<>(getManager().getGraveyard().getDicoGraveyard().get(getManager().getPlayerModel().getP2())));

        if(getManager().getPlayerModel().getReadyPlayer() != getManager().getPlayerModel().getP1()) {getManager().getPlayerModel().changePlayer();}
        setUpPieces();
        getManager().setDataManager(new XMLChessDataManager());
    }

    /**
     * Fonction permettant de mettre dans "lc" la liste des déplacements possibles d'une piece ou de bouger cette piece
     * @param mouseEvent : Contient les informations sur la case cliquée
     */
    @FXML
    public void caseClick(MouseEvent mouseEvent) {
        if ( ((StackPane) mouseEvent.getSource()).getChildren().size() > 1 && ((StackPane) mouseEvent.getSource()).getChildren().get(1) instanceof Rectangle && ((Rectangle)((StackPane) mouseEvent.getSource()).getChildren().get(1)).getFill() == FILL_OVERLAY) {
            try{
                Coordinate cc = ChessCoordinate.coordFromString(((StackPane) mouseEvent.getSource()).getId());
                var finish = movePiece(p,cc);
                cleanBlueCase();
                if (finish) {return;}
                getManager().getPlayerModel().changePlayer();

            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        } else {
            cleanBlueCase();
            String strCoord = ((StackPane) mouseEvent.getSource()).getId();
            try {
                Coordinate cc = ChessCoordinate.coordFromString(strCoord);
                p = getBoard().getDicoCoord().get(cc).get();
                if (p == null) {
                    return;
                }
                if(p.getPieceColor() == PieceColor.WHITE && getManager().getPlayerModel().getReadyPlayer() == getManager().getPlayerModel().getP1()
                        ||(p.getPieceColor() == PieceColor.BLACK && getManager().getPlayerModel().getReadyPlayer() == getManager().getPlayerModel().getP2() )){
                    showBlueCase();
                }

            } catch (Exception e) {
               e.printStackTrace();
                System.exit(1);
            }
        }
    }

    /**
     * Permet de "nettoyer" les case surlignées dans la vue
     */
    private void cleanBlueCase() {
        for (Coordinate c : lc) {
            int column = ChessCoordinate.listColumn.indexOf(c.getColumn());
            int row = c.getRow();
            (getViewController().getStackPane(column, row)).getChildren().set(INDEX_OVERLAY, new Rectangle(CASE_SIZE, CASE_SIZE, FILL_TRANSPARENT));
            lc = new ArrayList<>();
        }
    }

    /**
     * @param p : la piece à bouger
     * @param c : la coordonée ou veut aller la piece
     * @return retourne si la partie est fini
     */
    private boolean movePiece(Piece p, Coordinate c){
        if(p instanceof Pawn){
            if(((Pawn)p).isFirstMove()){
                ((Pawn)p).setFirstMove(false);
            }
        }
        Alert alert;
        getBoard().getDicoCoord().get(p.getCoordinate()).setValue(null);
        p.setCoordinate(c);
        Piece d = getBoard().getDicoCoord().get(c).getValue();
        if(d != null){

            alert=isKing(d);                                    // Si un des rois est mangés
            if(alert!=null){                                    // Fin de partie
                return true;//
            }
            if(getManager().getPlayerModel().getP1() == getManager().getPlayerModel().getReadyPlayer()){
                getManager().getGraveyard().tookPiece(getManager().getPlayerModel().getP2(),d);
            }
            else{
                getManager().getGraveyard().tookPiece(getManager().getPlayerModel().getP1(),d);
            }
        }
        getBoard().getDicoCoord().get(c).setValue(pawnPromoted(p,c));
        if(d != null){

            getManager().getHistoryModel().addHistory(p,d);
        }else{
            getManager().getHistoryModel().addHistory(p);
        }
        return false;

    }

    /**
     * Fonction qui gère la promotion d'un pion lorsque il arrive dans le camps adverse
     * @param d la piece prise
     * @param c la coordonée
     * @return retourne la piece séléctionnée dans l'Alert
     */
    private Piece pawnPromoted(Piece d, Coordinate c){
        Alert alert;
        alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(internationalization.I18n.getString("PawnPromotion"));
        ButtonType buttonGrave = new ButtonType(internationalization.I18n.getString("Graveyard"));
        alert.getButtonTypes().setAll(buttonGrave);

        ChoiceGraveyard dialog = new ChoiceGraveyard(d,getManager().getGraveyard().getDicoGraveyard().get(getManager().getPlayerModel().getReadyPlayer()));
        dialog.setTitle(internationalization.I18n.getString("Promoted"));
        dialog.setContentText(internationalization.I18n.getString("ChoosePiece"));

        if(d instanceof Pawn) {
            Optional<Piece> result;
            switch (d.getPieceColor()) {

                case WHITE:
                    if (c.getRow() == 8) {
                        result = dialog.showAndWait();
                        Piece p = choosePiece(result, dialog, d);
                        getManager().getGraveyard().rebornPiece(getManager().getPlayerModel().getP1(),p);
                        return p;
                    }

                case BLACK:
                    if (c.getRow() == 1) {
                        result = dialog.showAndWait();
                        Piece p = choosePiece(result, dialog, d);
                        getManager().getGraveyard().rebornPiece(getManager().getPlayerModel().getP2(),p);
                        return p;
                    }
            }
        }
        return d;
    }

    /**
     * Choix dans l'alert box d'une promotion de pion
     * @param res
     * @param dialog
     * @param d la piece
     * @return La piece choisie
     */
    private Piece choosePiece(Optional<Piece> res, ChoiceGraveyard dialog, Piece d){
        if(!res.isPresent()){ return null;}
        res.get().setCoordinate(d.getCoordinate());
        return res.get();
    }

    /**
     * Fonction qui regarde si le jeu est fini et si c'est le cas, envoie une alert box
     * @param d piece qui vient d'être prise
     * @return retourne une alert BOx
     */
    private Alert isKing(Piece d){
        if(d instanceof King){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            PieceColor win = PieceColor.WHITE;
            if(d.getPieceColor()== PieceColor.WHITE){
                win=PieceColor.BLACK;
            }
            alert.setTitle(internationalization.I18n.getString("The")+" "+win.toString()+" "+ internationalization.I18n.getString("Won"));
            ButtonType buttonTypeOne = new ButtonType(internationalization.I18n.getString("Restart"));
            ButtonType buttonTypeTwo = new ButtonType(internationalization.I18n.getString("Quit"));

            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

            Optional<ButtonType> result = alert.showAndWait();
            if (!result.isPresent()){ return null; }
            if (result.get() == buttonTypeOne){
                reset();
                alert.close();

            } else if (result.get() == buttonTypeTwo) {
                alert.close();
                reset();
                System.exit(0);
            }
            return alert;
        }
        return null;
    }

    /**
     * Affiche les case overlay
     */
    private void showBlueCase() {
        lc = p.getMoveFromBoard(getBoard());
        for (Coordinate c : lc) {
            int column = ChessCoordinate.listColumn.indexOf(c.getColumn());
            int row = c.getRow();
            (getViewController().getStackPane(column, row)).getChildren().set(INDEX_OVERLAY, new Rectangle(CASE_SIZE, CASE_SIZE, FILL_OVERLAY));
        }
    }

    /**
     * Met en place les images
     */
    public void setupImage() {
        for (Coordinate c : getBoard().getDicoCoord().keySet()) {
            int column = ChessCoordinate.listColumn.indexOf(c.getColumn());
            int row = c.getRow();
            ((HBox) getViewController().board.getChildren().get(7 - (row - 1))).getChildren().get(column).setId(c.toString());
            getBoard().getDicoCoord().get(c).addListener(getViewController().pieceListener);
            if (getBoard().getDicoCoord().get(c).get() == null)
            {
                getViewController().getStackPane(column, row).getChildren().add(new Rectangle(CASE_SIZE,CASE_SIZE, FILL_TRANSPARENT));
                continue;
            }
            ImageView image = new ImageView(new Image(getBoard().getDicoCoord().get(c).get().getImageURL()));
            image.setPreserveRatio(true);
            image.setFitWidth(CASE_SIZE);
            getViewController().getStackPane(column, row).getChildren().add(image);


        }
    }


}
